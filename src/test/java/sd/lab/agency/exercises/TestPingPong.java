package sd.lab.agency.exercises;

import sd.lab.agency.Environment;
import sd.lab.agency.impl.LocalEnvironment;
import sd.lab.exercises.PingAgent;
import sd.lab.exercises.PongAgent;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.test.ConcurrentTestHelper;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class TestPingPong {

    private static final Duration MAX_WAIT = Duration.ofSeconds(2);

    protected ConcurrentTestHelper test;
    protected Random rand;
    protected Environment mas;

    private final int testIndex;

    @Parameterized.Parameters
    public static Iterable<Integer> data() {
        return IntStream.range(0, 5).boxed().collect(Collectors.toList());
    }

    public TestPingPong(Integer i) {
        testIndex = i;
    }

    @Before
    public void setUp() throws Exception {
        test = new ConcurrentTestHelper();
        rand = new Random();
        // TODO notice that all agents are executed by a single thread in this test suite!
        mas = new LocalEnvironment(Executors.newSingleThreadExecutor());
    }

    @After
    public void tearDown() throws InterruptedException, ExecutionException, TimeoutException {
        mas.shutdown().awaitShutdown(MAX_WAIT);
    }

    // TODO readme
    @Test
    public void testPingPongExercise() throws Exception {
        final PingAgent ping = (PingAgent) mas.createAgent(PingAgent.class, "PingAgent", "testPingPongExercise-" + testIndex).start();

        mas.createAgent(PongAgent.class, "PongAgent", "testPingPongExercise-" + testIndex).start();

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(10, ping.getPongCount());
        Assert.assertTrue(ping.isPingTurn());
    }

}
