package sd.lab.agency.impl;

import sd.lab.linda.textual.TextualSpace;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class LocalEnvironment extends AbstractEnvironment {

    private final Map<String, TextualSpace> textualSpaces = new HashMap<>();

    public LocalEnvironment(ExecutorService engine, String name) {
        super(engine, name);
    }

    public LocalEnvironment(String name) {
        super(name);
    }

    public LocalEnvironment(ExecutorService engine) {
        super(engine);
    }

    public LocalEnvironment() {
    }

    @Override
    public TextualSpace getTextualSpace(String name) {
        return textualSpaces.computeIfAbsent(name, n -> TextualSpace.of(n, getEngine()));
    }
}
