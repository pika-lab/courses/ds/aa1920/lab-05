package sd.lab.agency.impl;

import sd.lab.agency.Agent;
import sd.lab.agency.AndThen;
import sd.lab.agency.Environment;

import java.time.Duration;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class BaseAgent implements Agent {

    protected enum States {
        CREATED, STARTED, RUNNING, PAUSED, STOPPED
    }

    private final String name;
    // TODO to be completed as soon as the agent stops
    private final CompletableFuture<Void> termination = new CompletableFuture<>();
    private States state = States.CREATED;
    private AndThen nextOperation = null;
    private Environment environment;

    protected BaseAgent(String name) {
        this.name = Optional.ofNullable(name).orElseGet(() -> getClass().getSimpleName() + "#" + System.identityHashCode(this));
    }

    private void ensureCurrentStateIs(States state) {
        ensureCurrentStateIs(EnumSet.of(state));
    }

    private void ensureCurrentStateIs(EnumSet<States> states) {
        if (!currentStateIsOneOf(states)) {
            throw new IllegalStateException("Illegal state: " + this.state + ", expected: " + states);
        }
    }

    private boolean currentStateIs(States state) {
        return Objects.equals(this.state, state);
    }

    private boolean currentStateIsOneOf(EnumSet<States> states) {
        return states.contains(this.state);
    }

    private void doStateTransition(AndThen whatToDo) {
        switch (state) {
            case CREATED:
                doStateTransitionFromCreated(whatToDo);
                break;
            case STARTED:
                doStateTransitionFromStarted(whatToDo);
                break;
            case RUNNING:
                doStateTransitionFromRunning(whatToDo);
                break;
            case PAUSED:
                doStateTransitionFromPaused(whatToDo);
                break;
            case STOPPED:
                doStateTransitionFromStopped(whatToDo);
                break;
            default: throw new IllegalStateException("Illegal state: " + state);
        }
    }

    protected void doStateTransitionFromCreated(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromStarted(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromRunning(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromPaused(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromStopped(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    // TODO notice che convention doBegin --> begin --> onBegin

    private void doBegin() {
        if (getEnvironment() == null) {
            throw new IllegalStateException();
        }
        ensureCurrentStateIs(States.STARTED);
        getEngine().submit(this::begin);
    }

    private void begin() {
        nextOperation = AndThen.CONTINUE;
        try {
            onBegin();
        } catch (Exception e) {
            nextOperation = onUncaughtError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    @Override
    public void onBegin() throws Exception {
        // it's a callback and it does nothing by default (must be inherited)
    }

    // TODO notice che convention doRun --> run --> onRun

    private void doRun() {
        ensureCurrentStateIs(EnumSet.of(States.PAUSED, States.RUNNING));
        getEngine().submit(this::run);
    }

    private void run() {
        // TODO implement me similarly to begin()
    }

    // TODO keep this abstract in order to force subclasses to provide some behaviour
    public abstract void onRun() throws Exception;

    // TODO notice che convention doRun --> run --> onRun

    private void doEnd() {
        // TODO implement me similarly to begin()
    }

    private void end() {
        // TODO implement me similarly to doBegin() and doRun()
    }

    @Override
    public void onEnd() throws Exception {
        // does nothing by default
    }

    @Override
    public AndThen onUncaughtError(Exception e) {
        // by default, it simply print stacktrace and then stops the agent (can be overridden)
        e.printStackTrace();
        return AndThen.STOP;
    }

    @Override
    public Agent start() {
        ensureCurrentStateIs(States.CREATED);
        // TODO provoke state transition from CREATED to STARTED
        return this;
    }

    @Override
    public Agent resume() {
        ensureCurrentStateIs(States.PAUSED);
        // TODO provoke state transition from PAUSED to STARTED
        return this;
    }

    @Override
    public Agent pause() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        // TODO make the agent remember that it should move into the PAUSED state after the current onRun() or onBegin()
        return this;
    }

    @Override
    public Agent stop() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        if (currentStateIs(States.PAUSED)) {
            doStateTransition(AndThen.STOP);
        } else {
            nextOperation = AndThen.STOP;
        }
        return this;
    }

    @Override
    public Agent restart() {
        ensureCurrentStateIs(EnumSet.complementOf(EnumSet.of(States.CREATED)));
        // TODO make the agent remember that it should move into the BEGIN state after the current onRun() or onBegin()
        return this;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = Objects.requireNonNull(environment);
    }

    protected final void log(Object format, Object... args) {
        System.out.printf("[" + getFullName() +"] " + format + "\n", args);
    }

    @Override
    public String getFullName() {
        return getLocalName() + "@" + (getEnvironment() != null ? getEnvironment().getName() : null);
    }

    @Override
    public String getLocalName() {
        return name;
    }

    @Override
    public Agent await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(duration.toMillis(), TimeUnit.MILLISECONDS);
        return this;
    }

    @Override
    public Agent await() throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        return this;
    }

}

