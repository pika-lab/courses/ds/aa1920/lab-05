package sd.lab.agency;

import sd.lab.linda.textual.TextualSpace;

import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Supplier;

public interface Environment {

    TextualSpace getTextualSpace(String name);

    <A extends Agent> A createAgent(Class<A> clazz, String name, Object... args);

    <A extends Agent> A registerAgent(A agent);

    Set<Agent> getAgents();

    String getName();

    Environment awaitAllAgentsStop(Duration duration) throws InterruptedException, ExecutionException, TimeoutException;

    Environment shutdown();

    Environment awaitShutdown(Duration duration) throws InterruptedException;

    ExecutorService getEngine();
}
