package sd.lab.exercises;

import sd.lab.agency.impl.BaseAgent;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;

import java.util.Objects;
import java.util.regex.Matcher;

public class PongAgent extends BaseAgent {

    private final String channelName;

    public PongAgent(String name, String channelName) {
        super(name);
        this.channelName = Objects.requireNonNull(channelName);
    }

    @Override
    public void onRun() throws Exception {
        stop();
    }

}
