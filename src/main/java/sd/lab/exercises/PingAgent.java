package sd.lab.exercises;

import sd.lab.agency.impl.BaseAgent;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;

import java.util.Objects;
import java.util.regex.Matcher;

// TODO notice me!
public class PingAgent extends BaseAgent {

    // WTF?! https://regex101.com/r/dGHAjw/1/
    private final RegexTemplate msgTemplate;

    private int pongCount = 0;
    private boolean pingTurn = true;
    private final String channelName;
    private TextualSpace channel;

    public PingAgent(String name, String channelName) {
        super(name);
        this.channelName = Objects.requireNonNull(channelName);
        // WTF?! https://regex101.com/r/dGHAjw/2
        this.msgTemplate = RegexTemplate.of(
                "^\\s*msg\\s*\\{\\s*to:\\s*" + name + "\\s*,\\s*content:\\s*(.*?)\\s*\\}\\s*$"
        );
    }

    @Override
    public void onBegin() throws Exception {
        channel = getEnvironment().getTextualSpace(channelName);
    }

    @Override
    public void onRun() throws Exception {
        if (pongCount >= 10) {
            channel.out("msg{to:PongAgent, content:stop}").thenAcceptAsync(this::onStopMessageWritten, getEngine());
            pause();
        } else if (pingTurn) {
            channel.out("msg{to:PongAgent, content:ping}").thenAcceptAsync(this::onPingWritten, getEngine());
            pause();
        } else {
            channel.in(msgTemplate).thenAcceptAsync(this::onMessageReceived, getEngine());
            pause();
        }
    }

    private void onStopMessageWritten(StringTuple tuple) {
        stop();
    }

    private void onPingWritten(StringTuple tuple) {
        log("Sent PING");
        pingTurn = false;
        resume();
    }

    private void onMessageReceived(StringTuple tuple) {
        log("Received message %s", tuple);
        final Matcher m = msgTemplate.getRegex().matcher(tuple.getValue());
        if (m.matches()) {
            final String content = m.group(1);
            if (content.equalsIgnoreCase("pong")) {
                log("Receive PONG");
                pongCount++;
                pingTurn = true;
                resume();
            } else {
                log("Invalid message. I'm out");
                stop();
            }
        } else {
            log("THIS SHOULD NEVER HAPPEN");
            stop();
        }
    }

    public int getPongCount() {
        return pongCount;
    }

    public boolean isPingTurn() {
        return pingTurn;
    }
}
